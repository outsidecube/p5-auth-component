const { codes } = require('../conf/config').config;
const { restart } = require('nodemon');
const authService = require('../services/auth.service');

class authController {
  static async login(req, res) {
    const { email, password } = req.body;

    if (!email || !password) {
      res
        .status(400)
        .send({ msg: 'Missing email or password' });
      return;
    }

    try {
      const result = await authService.login(email, password);

      return res.status(200).send(result);
    } catch (error) {
      console.log(`error login with ${email}, ${error}`);

      res.status(codes.serverError).send({
        msg: 'error on signin',
        dev_msg: `error: ${error}`,
      });
    }
  }

  static async verify(req, res) {
    const { token } = req.body;

    if (!token) {
      res
        .status(400)
        .send({ msg: 'Missing token to verify' });
      return;
    }

    try {
      const result = await authService.verify(token);

      if (!result)
        return res.status(401).send({
          msg: 'Invalid token',
        });
      else
        return res.status(200).send({
          msg: 'valid token',
        });
    } catch (error) {
      console.log(`error verify ${error}`);

      res.status(codes.serverError).send({
        msg: 'error on signin',
        dev_msg: `error: ${error}`,
      });
    }
  }
}

module.exports = authController;