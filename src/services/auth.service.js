const userDao = require('../dao/user.dao');
const jwt = require('jsonwebtoken');
const { privateKey, ttl } = require('../conf/config').tokenConfig;

class authService {
  static async login(email, password) {
    const result = await userDao.get(email, password);
    const userData = result[0];

    return await this.createToken(userData.id, userData.email, userData.role);
  }

  static async createToken(id, email, role) {
    const tokenData = { id, email, role };

    const generatedToken = await jwt.sign(tokenData, privateKey, {expiresIn: 60 * 60});

    return { token: generatedToken };
  }

  static async verify(token) {
    let valid=false;
    
    jwt.verify(token, privateKey, function (err) {
      if (err) {
        valid=false;
      } else {
        valid=true;
      }
    });

    return valid;
  }
}

module.exports = authService;