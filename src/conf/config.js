// Check for mandatory environment variables
const required = [
  'DB_MAIN_HOST',
  'DB_MAIN_USER',
  'DB_MAIN_PASS',
  'DB_MAIN_NAME',
];

required.forEach(param => {
    if (!process.env[param]) {
        throw new Error(`Environment parameter ${param} is missing`);
    }
});

const config = {
  env: process.env['NODE_ENV'],
  db: {
    host: process.env['DB_MAIN_HOST'],
    user: process.env['DB_MAIN_USER'],
    pass: process.env['DB_MAIN_PASS'],
    name: process.env['DB_MAIN_NAME'],
  },
  codes: {
    serverError: 500,
    badRequest: 400,
    success: 200,
  },
};

const mainDatabase = {
  host: process.env['DB_MAIN_HOST'],
  user: process.env['DB_MAIN_USER'],
  pass: process.env['DB_MAIN_PASS'],
  name: process.env['DB_MAIN_NAME'],
};

const tokenConfig = {
  privateKey: process.env['SECRET'],
  ttl: process.env['TTL']
};

module.exports = { config, tokenConfig, mainDatabase };