const { query } = require('../repositories/main.repository');

class userDao {
  static get(email, password) {
    const sql = `SELECT id, email, role FROM Usuario WHERE email = ? and password = ? `;

    return query(sql, [email, password]);
  }
}

module.exports = userDao;
