const express = require('express');
const authController = require('./src/controllers/auth.controller');
const router  = express.Router();

const version = 'v1';

router.post(`/${version}/login`, authController.login);
router.post(`/${version}/verify`, authController.verify);

module.exports = router;
